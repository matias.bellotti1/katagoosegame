Write a program that runs through the board spaces and prints the rules of the game according to the board space.
For all the spaces you must print Stay in space {{space you fell in}},
but for multiples of six print Move two spaces forward, and for space 6 add the rule The Bridge: Go to space 12.

Sample output:
Stay in space 1
Stay in space 2
Stay in space 3
Stay in space 4
Stay in space 5
The Bridge: Go to space 12
Stay in space 7
Stay in space 8
Stay in space 9
Stay in space 10
Stay in space 11
Move two spaces forward.
Stay in space 13
Stay in space 14
Stay in space 15
Stay in space 16
Stay in space 17
Move two spaces forward.
...etc