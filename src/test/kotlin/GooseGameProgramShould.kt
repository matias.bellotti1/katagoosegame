import org.junit.Assert.assertEquals
import org.junit.Test

class GooseGameProgramShould {

    lateinit var gooseGame: GooseGame
    lateinit var printLocketRules: String

    @Test
    fun `print Stay in space 3 when space is three`(){

        giveAgooseGame()

        whenPrintAspaceRule(3)

        thenPrintRuleSpaceThree()
    }


    @Test
    fun `print Stay in space 1 when space is one`(){

        giveAgooseGame()

        whenPrintAspaceRule(1)

        thenPrintRuleSpaceOne()
    }


    @Test
    fun `print Stay in space 2 when space is two`(){

        giveAgooseGame()

        whenPrintAspaceRule(2)

        thenPrintRuleSpaceTwo()
    }


    @Test
    fun `print he Bridge Go to space 12 when space is six`(){

        giveAgooseGame()

        whenPrintAspaceRule(6)

        thenPrintRuleSpaceSix()
    }


    @Test
    fun `print Move two spaces forward when space is multiple of six`(){

        giveAgooseGame()

        whenPrintAspaceRule(12)

        thenPrintRuleSpaceMultipleSix()
    }

    @Test
    fun `print Move two spaces forward when space is other multiple of six`(){

        giveAgooseGame()

        whenPrintAspaceRule(18)

        thenPrintRuleSpaceMultipleSix()
    }

    @Test
    fun `print The Hotel Stay for (miss) one turn when space is 19`(){

        giveAgooseGame()

        whenPrintAspaceRule(19)

        thenPrintRuleSpace19()
    }

    @Test
    fun `print The Well Wait until someone comes to pull you out - they then take your place when space is 31`(){

        giveAgooseGame()

        whenPrintAspaceRule(31)

        thenPrintRuleSpace31()
    }

    @Test
    fun `print The Maze Go back to space 39 when space is 42`(){

        giveAgooseGame()

        whenPrintAspaceRule(42)

        thenPrintRuleSpace42()
    }

    @Test
    fun `print The Prison Wait until someone comes to release you - they then take your place when space is 50`(){

        giveAgooseGame()

        whenPrintAspaceRule(50)

        thenPrintRuleThePrison()
    }

    @Test
    fun `print The Prison Wait until someone comes to release you - they then take your place when space is 51`(){

        giveAgooseGame()

        whenPrintAspaceRule(51)

        thenPrintRuleThePrison()
    }

    @Test
    fun `print The Prison Wait until someone comes to release you - they then take your place when space is 52`(){

        giveAgooseGame()

        whenPrintAspaceRule(52)

        thenPrintRuleThePrison()
    }

    @Test
    fun `print The Prison Wait until someone comes to release you - they then take your place when space is 53`(){

        giveAgooseGame()

        whenPrintAspaceRule(53)

        thenPrintRuleThePrison()
    }

    @Test
    fun `print The Prison Wait until someone comes to release you - they then take your place when space is 54`(){

        giveAgooseGame()

        whenPrintAspaceRule(54)

        thenPrintRuleThePrison()
    }

    @Test
    fun `print The Prison Wait until someone comes to release you - they then take your place when space is 55`(){

        giveAgooseGame()

        whenPrintAspaceRule(55)

        thenPrintRuleThePrison()
    }


    @Test
    fun `print Death Return your piece to the beginning - start the game again when space is 58`(){

        giveAgooseGame()

        whenPrintAspaceRule(58)

        thenPrintRuleSpace58()
    }

    @Test
    fun `print Finish you ended the game when space is 63`(){

        giveAgooseGame()

        whenPrintAspaceRule(63)

        thenPrintRuleSpace63()
    }

    @Test
    fun `print Move to space 53 and stay in prison for two turns when space is higher to 63`(){

        giveAgooseGame()

        whenPrintAspaceRule(64)

        thenPrintRuleSpaceHigherTo63()
    }

    @Test
    fun `print Move to space 53 and stay in prison for two turns when space is other higher to 63`(){

        giveAgooseGame()

        whenPrintAspaceRule(70)

        thenPrintRuleSpaceHigherTo63()
    }

    private fun thenPrintRuleSpaceHigherTo63() {
        assertEquals("Move to space 53 and stay in prison for two turns", printLocketRules)
    }

    private fun thenPrintRuleSpace63() {
        assertEquals("Finish: you ended the game", printLocketRules)
    }

    private fun thenPrintRuleSpace58() {
        assertEquals("Death: Return your piece to the beginning - start the game again", printLocketRules)
    }

    private fun thenPrintRuleThePrison() {
        assertEquals("The Prison: Wait until someone comes to release you - they then take your place", printLocketRules)
    }

    private fun thenPrintRuleSpace42() {
        assertEquals("The Maze: Go back to space 39", printLocketRules)
    }

    private fun thenPrintRuleSpace31() {
        assertEquals("The Well: Wait until someone comes to pull you out - they then take your place", printLocketRules)
    }

    private fun thenPrintRuleSpace19() {
        assertEquals("The Hotel: Stay for (miss) one turn", printLocketRules)
    }

    private fun thenPrintRuleSpaceSix() {
        assertEquals("The Bridge: Go to space 12", printLocketRules)
    }

    private fun thenPrintRuleSpaceMultipleSix() {
        assertEquals("Move two spaces forward.", printLocketRules)
    }

    private fun thenPrintRuleSpaceOne() {
        assertEquals("Stay in space 1", printLocketRules)
    }

    private fun thenPrintRuleSpaceTwo() {
        assertEquals("Stay in space 2", printLocketRules)
    }

    private fun thenPrintRuleSpaceThree() {
        assertEquals("Stay in space 3", printLocketRules)
    }

    private fun giveAgooseGame() {
        this.gooseGame = GooseGame()
    }

    private fun whenPrintAspaceRule(space: Int){
        printLocketRules = gooseGame.printSpaceRules(space)
    }
}
