class GooseGame {
    fun printSpaceRules(space: Int): String {

        if(space == 6) return "The Bridge: Go to space 12"

        if(space == 42) return "The Maze: Go back to space 39"

        if(space == 50 || space == 51 || space == 52 || space == 53 || space == 54 || space == 55) return "The Prison: Wait until someone comes to release you - they then take your place"

        if(space % 6 == 0) return "Move two spaces forward."

        if(space == 19) return "The Hotel: Stay for (miss) one turn"

        if(space == 31) return "The Well: Wait until someone comes to pull you out - they then take your place"

        if(space == 58) return "Death: Return your piece to the beginning - start the game again"

        if(space == 63) return "Finish: you ended the game"

        if(space > 63) return "Move to space 53 and stay in prison for two turns"

        return "Stay in space $space"
    }
}
